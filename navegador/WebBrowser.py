#!/usr/bin/env python
from gettext import gettext as _
import os
import gobject
import gtk
import pango
import webkit
from metodos import *
from WebToolbar import WebToolbar
from ContentPane import ContentPane

class WebBrowser(gtk.Window):
   
    def delete_event(self, widget, event):
        return gtk.TRUE

    def __init__(self):
        gtk.Window.__init__(self)

        toolbar = WebToolbar()
        content_tabs = ContentPane()
        content_tabs.connect("focus-view-title-changed", self._title_changed_cb, toolbar)
        content_tabs.connect("new-window-requested", self._new_window_requested_cb)
        toolbar.connect("load-requested", load_requested_cb, content_tabs)
        toolbar.connect("new-tab-requested", new_tab_requested_cb, content_tabs)
        toolbar.connect("view-source-mode-requested", view_source_mode_requested_cb, content_tabs)

        vbox = gtk.VBox(spacing=1)
        #vbox.pack_start(toolbar, expand=False, fill=False)
        vbox.pack_start(content_tabs)

        self.add(vbox)
        self.set_default_size(1024, 768)
        self.fullscreen()
        self.connect('destroy', destroy_cb, content_tabs)

        self.connect("delete_event", self.delete_event)

        self.show_all()

        #content_tabs.new_tab("https://portal.auxiliadorapredial.com.br")
	#content_tabs.new_tab("http://www.google.com")
        #content_tabs.new_tab("file://../index.html")
        print "file://"+self.getPath("index.html")
        content_tabs.new_tab("file://"+self.getPath("../index.html"))


    def getPath(self, arquivo):
        dir_atual = os.path.dirname(os.path.abspath(__file__))
        a = dir_atual+"/"+arquivo
        return a

    def openFile(self, arquivo):
        self.web.open("file://%s" % arquivo)

    def _new_window_requested_cb (self, content_pane, view):
        features = view.get_window_features()
        window = view.get_toplevel()

        scrolled_window = view.get_parent()
        if features.get_property("scrollbar-visible"):
            scrolled_window.props.hscrollbar_policy = gtk.POLICY_NEVER
            scrolled_window.props.vscrollbar_policy = gtk.POLICY_NEVER

        isLocationbarVisible = features.get_property("locationbar-visible")
        isToolbarVisible = features.get_property("toolbar-visible")
        if isLocationbarVisible or isToolbarVisible:
            toolbar = WebToolbar(isLocationbarVisible, isToolbarVisible)
            scrolled_window.get_parent().pack_start(toolbar, False, False, 0)

        #window.set_default_size(features.props.width, features.props.height)
        window.set_default_size(1024, 768)
	window.move(features.props.x, features.props.y)

        window.show_all()
        return True

    def _title_changed_cb (self, tabbed_pane, frame, title, toolbar):
        if not title:
           title = frame.get_uri()
        self.set_title(_("PyWebKitGtk - %s") % title)
        load_committed_cb(tabbed_pane, frame, toolbar)
