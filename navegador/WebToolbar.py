#!/usr/bin/env python
from gettext import gettext as _

import gobject
import gtk
import pango
import webkit

class WebToolbar(gtk.Toolbar):

    __gsignals__ = {
        "load-requested": (gobject.SIGNAL_RUN_FIRST,
                           gobject.TYPE_NONE,
                           (gobject.TYPE_STRING,)),
        "new-tab-requested": (gobject.SIGNAL_RUN_FIRST,
                              gobject.TYPE_NONE, ()),
        "view-source-mode-requested": (gobject.SIGNAL_RUN_FIRST,
                                       gobject.TYPE_NONE,
                                       (gobject.TYPE_BOOLEAN, ))
        }

    def __init__(self, location_enabled=True, toolbar_enabled=True):
        gtk.Toolbar.__init__(self)

        # location entry
        if location_enabled:
            self._entry = gtk.Entry()
            self._entry.connect('activate', self._entry_activate_cb)
            entry_item = gtk.ToolItem()
            entry_item.set_expand(True)
            entry_item.add(self._entry)
            self._entry.show()
            self.insert(entry_item, -1)
            entry_item.show()

        # add tab button
        if toolbar_enabled:
            addTabButton = gtk.ToolButton(gtk.STOCK_ADD)
            addTabButton.connect("clicked", self._add_tab_cb)
            self.insert(addTabButton, -1)
            addTabButton.show()

            viewSourceItem = gtk.ToggleToolButton(gtk.STOCK_PROPERTIES)
            viewSourceItem.set_label("View Source Mode")
            viewSourceItem.connect('toggled', self._view_source_mode_cb)
            self.insert(viewSourceItem, -1)
            viewSourceItem.show()
            
    def location_set_text (self, text):
        self._entry.set_text(text)

    def _entry_activate_cb(self, entry):
        self.emit("load-requested", entry.props.text)

    def _add_tab_cb(self, button):
        self.emit("new-tab-requested")

    def _view_source_mode_cb(self, button):
        self.emit("view-source-mode-requested", button.get_active())
