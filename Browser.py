#!/usr/bin/env python
import pygtk
import gtk
import webkit
import os

class Browser:    

    def getPath(self, arquivo):
        dir_atual = os.path.dirname(os.path.abspath(__file__))
        a = dir_atual+"/"+arquivo
        return a

    def openFile(self, arquivo):
        self.web.open("file://%s" % arquivo)

    def delete_event(self, widget, event):
        return gtk.TRUE

    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_size_request(1366, 768)
        self.window.fullscreen()
        self.window.set_border_width(5)
        self.window.set_title("Titulo da janela")
        self.window.set_position(gtk.WIN_POS_CENTER)
                
        self.window.connect("delete_event", self.delete_event)
 	
        self.vbox = gtk.VBox(False, 0)
        self.window.add(self.vbox)
        self.vbox.show()

	self.notebook = gtk.Notebook()
        self.vbox.pack_start(self.notebook)
        self.notebook.show()

        self.scroller = gtk.ScrolledWindow()
        self.vbox.pack_start(self.scroller)

        self.web = webkit.WebView()
        self.scroller.add(self.web)
        
        self.window.show_all()
        
        self.openFile(self.getPath("index.html"))

