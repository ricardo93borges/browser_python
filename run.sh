#!/bin/bash

#rsync -zvr root@10.77.100.162:/opt/navegador/ /opt/navegador/

BASEDIR=$(dirname $0)

$BASEDIR/get_images.py

until $BASEDIR/navegador/main.py; do
    echo "'run.py' crashed with exit code $?. Restarting..." >&2
    sleep 10
done

