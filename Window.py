#!/usr/bin/env python
import pygtk
import gtk
import webkit
import os

class Window:

    def getPath(arquivo):
        dir_atual = os.path.dirname(os.path.abspath(__file__))
        a = dir_atual+"arquivo"
        return a

    def openFile(arquivo):
        web.open("file://%s" % arquivo)

    def abrirPagina1(self, widget):
        self.web.open("http://www.iporto.com.br")
	#self.web.open("file://index.html")    

    def abrirPagina2(self, widget):
	self.web.open("http://www.adviceit.com.br")

    def delete_event(self, widget, event):
	return gtk.TRUE

    def minimized_event(self, widget, event):
	if event.changed_mask & gtk.gdk.WINDOW_STATE_ICONIFIED:
		if event.new_window_state & gtk.gdk.WINDOW_STATE_ICONIFIED:
			return gtk.TRUE
   
    def __init__(self):

		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_size_request(500, 350)
		#self.window.maximize()
		self.window.fullscreen()
		#self.window.set_resizable(False)
		self.window.set_border_width(10)
		self.window.set_title("Titulo da janela")
		self.window.set_position(gtk.WIN_POS_CENTER)
		#self.window.maximize()
		#self.window.connect("delete_event", lambda w,e: gtk.main_quit())
		self.window.connect("delete_event", self.delete_event)  
		self.window.connect("window-state-event", self.minimized_event) 
		#monta e adiciona uma box vertical na janela
		self.vbox = gtk.VBox(False, 0)
		self.window.add(self.vbox)
		self.vbox.show()
		#box_img
		self.box_img = gtk.HBox()
		self.vbox.pack_start(self.box_img, False)
		self.box_img.show()
		#imagem
		self.imagem = gtk.Image()
		self.imagem.set_from_file("img.png")
		self.box_img.pack_start(self.imagem)
		self.imagem.show()
		
		#monta e adiciona uma box para label na box vertical
		self.label_box = gtk.HBox()
		#self.vbox.pack_start(self.label_box, False)
		#self.label_box.show()
		    
		#monta e adiciona uma box horizontal na box vertical
		self.hbox = gtk.HBox()
		self.vbox.pack_start(self.hbox, False)
		self.hbox.show()
		    
		#monta e adiciona uma label na box para a label
		self.label = gtk.Label("Nome da aplicacao: ")
		self.label.set_justify(gtk.JUSTIFY_CENTER)
		self.label_box.pack_start(self.label, False, False, 6)
		self.label.show()
		    
		#monta e adiciona um scroller na box vertical
		self.scroller = gtk.ScrolledWindow()
		self.vbox.pack_start(self.scroller)
		    
		#monta um widget para exibir os sites
		self.web = webkit.WebView()
		self.scroller.add(self.web)
		
                #self.label2 = gtk.Label("Aguarde...")
                #self.label2.set_justify(gtk.JUSTIFY_CENTER)
                #self.web.pack_start(self.label2, False, False, 6)
                #self.label2.show()  
  
		#monta os botoes 
		self.btn1 = gtk.Button("Botao A")
		self.btn1.connect("clicked", self.abrirPagina1)
		    
		self.btn2 = gtk.Button("Botao B")
		self.btn2.connect("clicked", self.abrirPagina2)
		    
		#adiciona os botoes a box horizontal
		self.hbox.pack_start(self.btn1)
		self.hbox.pack_start(self.btn2)
		#self.hbox.pack_start(self.imagem)
		    
		#mostra os botoes
		self.btn1.show()
		self.btn2.show()
		    
		self.window.show_all()
