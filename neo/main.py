#!/usr/bin/env python
from gettext import gettext as _
import gobject
import gtk
import pango
import webkit
from WebBrowser import WebBrowser

def main():
    gtk.main()
    return -1
	
if __name__ == "__main__":
    webbrowser = WebBrowser()
    main()
