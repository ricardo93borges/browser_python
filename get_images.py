#!/usr/bin/env python

import os

dir_atual = os.path.dirname(os.path.abspath(__file__))
print dir_atual
images = []
ext = ['jpg', 'jpeg', 'png']
#files = os.listdir('img/slides')

#files = [f for f in os.listdir('img/slides') if os.isfile(join('img/slides', f))]

for file in os.listdir(dir_atual+'/img/slides'):
    f = file.split('.')
    if f[-1] in ext:
        images.append(file)
  

print images


s = open(dir_atual+'/slide.html', 'w')

s.write("\
	<html>\
	<head>\
		<script src='js/jquery.min.js'></script>\
		<title>Slides</title>\
		<style>\
			.fadein {position:relative; width:500px; height:332px; }\
			.fadein img { position:absolute; left:0; top:0; }\
		</style>\
	</head>\
	<body>\
	<div class='fadein'>\
")


for img in images:
    s.write("<img src='img/slides/"+img+"' width='1330'/>\n")

s.write("\
</div>\
	</body>\
	<script>\
	$('document').ready(function(){$(function(){$('.fadein img:gt(0)').hide();setInterval(function(){$('.fadein :first-child').fadeOut(2000).next('img').fadeIn(2000).end().appendTo('.fadein')},6000)})})\
	</script>\
	</html>\
")

s.close()



